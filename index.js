/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// For Number One

const addFunction = (a, b) => {
    console.log(a + b);
  };
  console.log("The sum of 5 and 3 is:");
  addFunction(5, 3);
  
  const minusFunction = (a, b) => {
    console.log(a - b);
  };
  console.log("The difference of 3 and 1 is:");
  minusFunction(3, 1);
  
  // For Number Two
  
  const multiplyFunction = (a, b) => {
    const result = a * b;
    return result;
  };
  console.log("The product of 2 and 3 is:");
  const product = multiplyFunction(2, 3);
  console.log(product);
  
  const divideFunction = (a, b) => {
    const result = a / b;
    return result;
  };
  console.log("The sum of 15 and 5 is:");
  const quotient = divideFunction(15, 5);
  console.log(quotient);
  
  // For Number Three
  
  const areaFunction = (radius) => {
    let area = Math.PI * radius ** 2;
    return area;
  };
  console.log("The result of getting the are of a circle with 2 radius is:");
  const circleArea = areaFunction(2);
  console.log(circleArea);
  
  // For Numbe Four
  
  const array = [5, 10, 15, 20];
  
  const averageFunction = (arr) => {
    const result = arr.reduce((a, b) => a + b, 0) / arr.length;
    return result;
  };
  console.log("The average of 5, 10, 15 and 20 is:");
  const averageVar = averageFunction(array);
  console.log(averageVar);
  
  // For Number 5
  
  const passedFunction = (myScore, totalScore) => {
    const precentage = (myScore / totalScore) * 100;
    const isPassed = precentage > 75 ? true : false;
    return isPassed;
  };
  console.log("Is 80/100 a passing score?");
  const isPassingScore = passedFunction(80, 100);
  console.log(isPassingScore);
  